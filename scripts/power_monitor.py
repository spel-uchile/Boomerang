from datetime import datetime
from ina260.controller import Controller
from numpy import zeros, savetxt
import os
import sys

if (len(sys.argv) < 1):
    sys.exit("Missing argument: Number of samples")
elif (len(sys.argv) < 2):
    sys.exit("Missing argument: Power threshold for warnings in Watts")

nsamples = int(sys.argv[1])
threshold = float(sys.argv[2])
naverage = 10
average_factor = 1/naverage
c = Controller(address=0x40)
date0 = datetime.utcnow()
data = zeros((nsamples, 4))

for sample in data:
    voltage = 0.0
    current = 0.0
    power = 0.0
    sample[0] = float(datetime.utcnow().strftime("%s.%f"))
    for i in range(naverage):
        voltage += c.voltage()
        current += c.current()
        power += c.power()
    sample[1] = voltage*average_factor
    sample[2] = current*average_factor
    sample[3] = power*average_factor
    if (sample[3] > threshold):
        print(f"Warning (power consumption): {sample[1]:.2f} V, {sample[2]:.2f} A, {sample[3]:.2f} W")

user = os.environ["USER"]
date_name = date0.strftime("%y%m%d_%H%M%S")
path = f"/home/{user}/Documents/POW/pow_{date_name}.csv"
savetxt(path, data, delimiter=",", fmt="%f")
os.system(f"xz -9 {path}")
