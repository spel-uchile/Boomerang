/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *     Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdRPI.h"
#include "app/system/taskPIX.h"

static const char *tag = "\e[38;2;255;217;102mPIX\x1b[0m";

void taskPIX(void *param) {
    osDelay(50);
    LOGI(tag, "Started");
    int sec_to_min = 60;
    portTick xLastWakeTime = osTaskGetTickCount();
    portTick task_period = 60*60*1000; //Task period in [ms]
    while (1) {
        int64_t t0 = dat_get_time();
        osDelay(3000*sec_to_min);
	cmd_t *export_cmd = cmd_build_from_str("rpi_system 10 export LD_LIBRARY_PATH=/home/pi/minipix/API_PIXet_RPi");
        cmd_send(export_cmd);
	LOGI(tag, "Turning on the USB hub");
        cmd_t *usb_on = cmd_build_from_str("rpi_system 10 sudo sh ~/Boomerang/powerup_usbhub.sh");
        cmd_send(usb_on);
        LOGI(tag, "Finished turning on the USB hub");
	/*
        while (dat_get_time() - t0 < 18000*sec_to_min) {
            cmd_t *pix_cmd = cmd_build_from_str("rpi_system 10 python3 ~/minipix/scripts/adv_acq.py frame >&- 2>&- &");
            cmd_send(pix_cmd);
	    LOGI(tag, "Measure radiation");
	    osDelay(20000);
	}
	*/
        LOGI(tag, "Start measuring radiation");
        cmd_t *pix_cmd = cmd_build_from_str("rpi_system 10 python3 ~/minipix/scripts/adv_acq.py frame >&- 2>&- &");
        cmd_send(pix_cmd);
	osDelay(15000*sec_to_min);
	LOGI(tag, "Turning off the USB hub");
	cmd_t *usb_off = cmd_build_from_str("rpi_system 10 sudo sh ~/Boomerang/powerdown_usbhub.sh");
	cmd_send(usb_off);
	LOGI(tag, "Finished turning off the USB hub");
	osTaskDelayUntil(&xLastWakeTime, task_period);
    }
}
