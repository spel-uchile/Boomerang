/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *     Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdRPI.h"
#include "app/system/taskSWT.h"

static const char *tag = "\e[38;2;0;0;0m\e[48;2;147;196;125mSWT\x1b[0m";

void taskSWT(void *param) {
    osDelay(50);
    LOGI(tag, "Started");
    int sec_to_min = 60;
    portTick xLastWakeTime = osTaskGetTickCount();
    portTick task_period = 60*60*1000; //Task period in [ms]
    while (1) {
        int64_t t0 = dat_get_time();
	osDelay(1800*sec_to_min);
        cmd_t *switch_cmd = cmd_build_from_str("rpi_system 10 python3 ~/Boomerang/scripts/board_switch.py >&- 2>&- &");
	LOGI(tag, "Turned on the board switch");
        cmd_send(switch_cmd);
	osDelay(17000*sec_to_min);
	LOGI(tag, "Turned off the board switch");
        osTaskDelayUntil(&xLastWakeTime, task_period);
    }
}
