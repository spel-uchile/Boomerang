#!/usr/bin/env bash
printf "Updating the Boomerang software\n"
git pull
cp -r apps/boomerapp ../suchai-flight-software/apps/
cp build.sh ../suchai-flight-software/build.sh

printf "Updating the Star Tracker (STT) software\n"
cd ../suchai-framework-stt/
git pull
cd ../suchai-flight-software/
cp ../suchai-framework-stt/apps/stt-suchai3/src/system/cmdSTT.c apps/boomerapp/src/system/cmdSTT.c
cp ../suchai-framework-stt/apps/stt-suchai3/src/system/cmdAUX.c apps/boomerapp/src/system/cmdAUX.c
cp ../suchai-framework-stt/apps/stt-suchai3/include/app/system/cmdSTT.h apps/boomerapp/include/app/system/cmdSTT.h
cp ../suchai-framework-stt/apps/stt-suchai3/include/app/system/cmdAUX.h apps/boomerapp/include/app/system/cmdAUX.h
cp -r ../suchai-framework-stt/stt_data .
cp -r ../suchai-framework-stt/sample_pics .
cp -r ../suchai-framework-stt/tracking_data .
cp -r ../suchai-framework-stt/apps/stt-suchai3/stt_python_scripts apps/boomerapp/

printf "Updating the SUCHAI software Template\n"
cd ../suchai-software-template/
git pull
cd ../suchai-flight-software/
cp ../suchai-software-template/apps/simple/src/system/cmdAPP.c apps/boomerapp/src/system/cmdAPP.c
cp ../suchai-software-template/apps/simple/include/app/system/cmdAPP.h apps/boomerapp/include/app/system/cmdAPP.h

printf "Updating the SUCHAI software grafeno\n"
cd ../suchai-software-grafeno/
git pull
cd ../suchai-flight-software/
cp ../suchai-software-grafeno/apps/grafeno/src/system/cmdGRAF.c apps/boomerapp/src/system/cmdGRAF.c
cp ../suchai-software-grafeno/apps/grafeno/include/app/system/cmdGRAF.h apps/boomerapp/include/app/system/cmdGRAF.h

printf "Updating the PCO software\n"
cd ../pco-iss/
git pull
cd ../suchai-flight-software/
cp ../pco-iss/apps/pco_app/src/system/cmdPCO.c apps/boomerapp/src/system/cmdPCO.c
cp ../pco-iss/apps/pco_app/include/app/system/cmdPCO.h apps/boomerapp/include/app/system/cmdPCO.h

printf "Updating the Camera Multiplexer software\n"
cd ../multiplexer_camera/
git pull
cd ../suchai-flight-software/

printf "Updating the Extremophiles software\n"
cd ../extremofilos_iss/
git pull
cd ../suchai-flight-software/

printf "Updating the MiniPix software\n"
cd ../minipix/
git pull
cd ../suchai-flight-software/

printf "Updating the ADCS software\n"
cd ../suchai-software-adcs-test/
git pull
cd ../suchai-flight-software/
cp ../suchai-software-adcs-test/apps/adcs_app/src/system/cmdADCS.c apps/boomerapp/src/system/cmdADCS.c
cp ../suchai-software-adcs-test/apps/adcs_app/src/system/taskADCS.c apps/boomerapp/src/system/taskADCS.c
cp ../suchai-software-adcs-test/apps/adcs_app/src/system/taskOrbit.c apps/boomerapp/src/system/taskOrbit.c
cp -r ../suchai-software-adcs-test/apps/adcs_app/src/system/eop_data/ apps/boomerapp/src/system/eop_data/
cp ../suchai-software-adcs-test/apps/adcs_app/include/app/system/cmdADCS.h apps/boomerapp/include/app/system/cmdADCS.h
cp ../suchai-software-adcs-test/apps/adcs_app/include/app/system/taskADCS.h apps/boomerapp/include/app/system/taskADCS.h
cp ../suchai-software-adcs-test/apps/adcs_app/include/app/system/taskOrbit.h apps/boomerapp/include/app/system/taskOrbit.h
cp -r ../suchai-software-adcs-test/apps/adcs_app/include/app/system/eop_data/ apps/boomerapp/include/app/system/eop_data/

printf "Updating the RPI Commands\n"
cd ../RPI_Commands/
git pull
cd ../suchai-flight-software/
cp ../RPI_Commands/check_frames.sh check_frames.sh
cp ../RPI_Commands/gen_cmds.py gen_cmds.py
cp ../RPI_Commands/apps/rpiapp/src/system/cmdRPI.c apps/boomerapp/src/system/cmdRPI.c
cp ../RPI_Commands/apps/rpiapp/include/app/system/cmdRPI.h apps/boomerapp/include/app/system/cmdRPI.h

printf "Updating the software for the magnetometer\n"
cd ../mag-plus-jmdp/
git pull
cd ../suchai-flight-software/

printf "Updating the USB hub controller\n"
cd ../uhubctl/
git pull
make
sudo make install
cd ../suchai-flight-software/

printf "Updating the SUCHAI flight software\n"
git pull
