/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *     Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdRPI.h"
#include "app/system/taskSTT.h"

static const char *tag = "\e[38;2;0;0;0m\e[48;2;255;255;255mSTT\x1b[0m";

void taskSTT(void *param) {
    osDelay(50);
    LOGI(tag, "Started");
    int sec_to_min = 60;
    portTick xLastWakeTime = osTaskGetTickCount();
    portTick task_period = 24*60*60*1000; //Task period in [ms]
    while (1) {
        int64_t t0 = dat_get_time();
	osDelay(20000*sec_to_min);
	LOGI(tag, "Start running the STT algorithm");
        cmd_t *stt_cmd = cmd_build_from_str("stt_solve_lis_iss 1 50");
        cmd_send(stt_cmd);
	LOGI(tag, "Finished running the STT algorithm");
        osTaskDelayUntil(&xLastWakeTime, task_period);
    }
}
