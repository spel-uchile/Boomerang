/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *    Copyright 2024, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *    Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/cmdAPP.h"
#include "app/system/cmdPCO.h"
#include "app/system/cmdSTT.h"
#include "app/system/cmdAUX.h"
#include "app/system/cmdGRAF.h"
#include "app/system/cmdADCS.h"
#include "app/system/cmdRPI.h"
#include "app/system/taskADCS.h"
#include "app/system/taskOrbit.h"
#include "app/system/taskCAM.h"
#include "app/system/taskEXT.h"
#include "app/system/taskGRA.h"
#include "app/system/taskMAG.h"
#include "app/system/taskLAS.h"
#include "app/system/taskLIQ.h"
#include "app/system/taskPIX.h"
#include "app/system/taskMOT.h"
#include "app/system/taskPCO.h"
#include "app/system/taskRAD.h"
#include "app/system/taskSWT.h"
#include "app/system/taskSTT.h"
#include "app/system/taskPOW.h"
#include "app/system/eop_data/earth_nutation.h"
#include "app/system/eop_data/eop_data_coeff.h"
#ifdef RPI
#include "csp_if_i2c_uart.h"
#endif

static char *tag = "app_main";

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params) {
    /** Include app commands */
    cmd_app_init();

    /** Include PCO commands */
    cmd_pco_init();

    /** Include STT commands */
    cmd_stt_init();

    /** Include STT auxiliary commands */
    cmd_aux_init();

    /** Include GRAF commands */
    cmd_graf_init();

    /** Include ADCS commands */
    cmd_adcs_init();

    /** Include RPI commands */
    cmd_rpi_init();

    /** Initialize custom CSP interfaces */
#if defined(X86)
    csp_add_zmq_iface(SCH_COMM_NODE);
#elif defined(RPI)
    csp_i2c_uart_init(SCH_COMM_NODE, 0, 19200);
    csp_rtable_set(8, 2, &csp_if_i2c_uart, 5); // Traffic to GND (8-15) via I2C to TRX node
    csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_i2c_uart, CSP_NODE_MAC); // Rest of the traffic to I2C using node i2c address
#endif

    /** Init housekeeping task */
    int t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, NULL, 2, NULL);
    if(t_ok != 0) LOGE("boomerapp-app", "Task housekeeping not created!");

    /** Init switch task */
    int t_ok_swt = osCreateTask(taskSWT, "switch", 1024, NULL, 2, NULL);
    if(t_ok_swt != 0) LOGE("boomerapp-app", "Task SWT not created!");

    /** Init ADCS task */
    int t_ok_adcs = osCreateTask(taskADCS, "adcs", 2048, NULL, 2, NULL);
    if(t_ok_adcs != 0) LOGE("boomerapp-app", "Task ADCS not created!");

    /** Init orbit task */
    int t_ok_orbit = osCreateTask(taskOrbit, "orbit", 2048, NULL, 3, NULL);
    if(t_ok_orbit != 0) LOGE("boomerapp-app", "Task Orbit not created!");

    /** Init CAM task */
    int t_ok_cam = osCreateTask(taskCAM, "cam", 2048, NULL, 3, NULL);
    if(t_ok_cam != 0) LOGE("boomerapp-app", "Task CAM not created!");

    /** Init EXT task */
    int t_ok_ext = osCreateTask(taskEXT, "ext", 2048, NULL, 3, NULL);
    if(t_ok_ext != 0) LOGE("boomerapp-app", "Task EXT not created!");

    /** Init GRA task */
    int t_ok_gra = osCreateTask(taskGRA, "gra", 2048, NULL, 3, NULL);
    if(t_ok_gra != 0) LOGE("boomerapp-app", "Task GRA not created!");

    /** Init MAG task */
    int t_ok_mag = osCreateTask(taskMAG, "mag", 2048, NULL, 3, NULL);
    if(t_ok_mag != 0) LOGE("boomerapp-app", "Task MAG not created!");

    /** Init LAS task */
    int t_ok_las = osCreateTask(taskLAS, "las", 2048, NULL, 3, NULL);
    if(t_ok_las != 0) LOGE("boomerapp-app", "Task LAS not created!");

    /** Init LIQ task */
    int t_ok_liq = osCreateTask(taskLIQ, "liq", 2048, NULL, 3, NULL);
    if(t_ok_liq != 0) LOGE("boomerapp-app", "Task LIQ not created!");

    /** Init PIX task */
    int t_ok_pix = osCreateTask(taskPIX, "pix", 2048, NULL, 3, NULL);
    if(t_ok_pix != 0) LOGE("boomerapp-app", "Task PIX not created!");

    /** Init MOT task */
    int t_ok_mot = osCreateTask(taskMOT, "mot", 2048, NULL, 3, NULL);
    if(t_ok_mot != 0) LOGE("boomerapp-app", "Task MOT not created!");

    /** Init PCO task */
    int t_ok_pco = osCreateTask(taskPCO, "pco", 2048, NULL, 3, NULL);
    if(t_ok_pco != 0) LOGE("boomerapp-app", "Task PCO not created!");

    /** Init RAD task */
    int t_ok_rad = osCreateTask(taskRAD, "rad", 2048, NULL, 3, NULL);
    if(t_ok_rad != 0) LOGE("boomerapp-app", "Task RAD not created!");

    /** Init STT task */
    int t_ok_stt = osCreateTask(taskSTT, "stt", 2048, NULL, 3, NULL);
    if(t_ok_stt != 0) LOGE("boomerapp-app", "Task STT not created!");

    /** Init POW task */
    int t_ok_pow = osCreateTask(taskPOW, "pow", 2048, NULL, 3, NULL);
    if(t_ok_pow != 0) LOGE("boomerapp-app", "Task POW not created!");
}

int main(void) {
    /** Call framework main, shouldn't return */
    suchai_main();
}
