#!/usr/bin/env bash

version=$(cat /proc/cpuinfo | grep Raspberry | awk '{print $5}')
if [ $version = '4' ]; then
	uhubctl -l 1-1 -a 0 && uhubctl -l 2 -a 0
elif [ $version = '5' ]; then
	uhubctl -l 1 -a 0 && uhubctl -l 3 -a 0
else
	uhubctl -l 1-1 -p 2 -a 0
fi
