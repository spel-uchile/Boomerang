/**
 * @file  dataSchema.h
 * @author Matias Vidal Valladares - matias.vidal.v@gmail.com
 * @date 2024
 * @copyright GNU GPL v3
 *
 * This header contains structs symbolizing the schema of data.
 */


#ifndef REPO_DATA_SCHEMA_H
#define REPO_DATA_SCHEMA_H

#include "suchai/log_utils.h"
#include "suchai/storage.h"

///**
// * Struct for storing a single timed command, set to execute in the future.
// */
//typedef struct __attribute__((packed)) fp_entry {
//    int unixtime;               ///< Unix-time, sets when the command should next execute
//    char* cmd;                  ///< Command to execute
//    char* args;                 ///< Command's arguments
//    int executions;             ///< Amount of times the command will be executed per periodic cycle
//    int periodical;             ///< Period of time between executions
//} fp_entry_t;

/**
 * Enum constants for dynamically identifying system status fields at execution time.
 *
 * Also permits adding new status variables cheaply, by generalizing both the
 * dat_set_system_var and dat_get_system_var functions.
 *
 * The dat_status_last_address constant serves only for comparison when looping through all
 * system status values. For example:
 *
 * @code
 * for (dat_status_address_t i = 0; i < dat_status_last_address; i++)
 * {
 * // some code using i.
 * }
 * @endcode
 *
 * @see dat_status_t
 * @seealso dat_set_system_var
 * @seealso dat_get_system_var
 */
typedef enum dat_status_address_enum {
    /// OBC: On board computer related variables.
    dat_obc_opmode = 0,           ///< General operation mode
    dat_obc_last_reset,           ///< Last reset source
    dat_obc_hrs_alive,            ///< Hours since first boot
    dat_obc_hrs_wo_reset,         ///< Hours since last reset
    dat_obc_reset_counter,        ///< Number of reset since first boot
    dat_obc_sw_wdt,               ///< Software watchdog timer counter
    dat_obc_temp_1,               ///< Temperature value of the first sensor
    dat_obc_executed_cmds,        ///< Total number of executed commands
    dat_obc_failed_cmds,          ///< Total number of failed commands

    /// RTC: Rtc related variables
    dat_rtc_date_time,            ///< RTC current unix time

    /// COM: Communications system variables.
    dat_com_count_tm,             ///< Number of Telemetries sent
    dat_com_count_tc,             ///< Number of received Telecommands
    dat_com_last_tc,              ///< Unix time of the last received Telecommand

    /// FPL: Flight plan related variables
    dat_fpl_last,                 ///< Last executed flight plan (unix time)
    dat_fpl_queue,                ///< Flight plan queue length

    /// Memory: Current payload memory addresses
    dat_drp_idx_temp,             ///< Temperature data index
    dat_drp_stt,                  /// TODO: cambiar por dat_drp_idx_stt

    /// Memory: Current send acknowledge data
    dat_drp_ack_temp,             ///< Temperature data acknowledge
    dat_drp_ack_stt,              ///< STT data acknowledge
    dat_drp_ack_stt_exp_time,
    dat_drp_ack_stt_gyro,

    /// STT: Star Tracker's system variables
    dat_drp_stt_exp_time,
    dat_drp_stt_gyro,

    /// ADS: Attitude determination system
    dat_ads_omega_x,              ///< Gyroscope acceleration value along the x axis
    dat_ads_omega_y,              ///< Gyroscope acceleration value along the y axis
    dat_ads_omega_z,              ///< Gyroscope acceleration value along the z axis
    dat_ads_ekf_omega_x,          ///< Gyroscope acceleration value along the x axis
    dat_ads_ekf_omega_y,          ///< Gyroscope acceleration value along the y axis
    dat_ads_ekf_omega_z,          ///< Gyroscope acceleration value along the z axis
    dat_ads_ekf_bias_x,           ///< bias Gyroscope acceleration value along the x axis
    dat_ads_ekf_bias_y,           ///< bias Gyroscope acceleration value along the y axis
    dat_ads_ekf_bias_z,           ///< bias Gyroscope acceleration value along the z axis
    dat_tgt_omega_x,              ///< Target acceleration value along the x axis
    dat_tgt_omega_y,              ///< Target acceleration value along the y axis
    dat_tgt_omega_z,              ///< Target acceleration value along the z axis
    dat_ads_mag_x,                ///< Magnetometer value along the x axis
    dat_ads_mag_y,                ///< Magnetometer value along the y axis
    dat_ads_mag_z,                ///< Magnetometer value along the z axis
    dat_ads_pos_x,                ///< Satellite orbit position x (ECI)
    dat_ads_pos_y,                ///< Satellite orbit position y (ECI)
    dat_ads_pos_z,                ///< Satellite orbit position z (ECI)
    dat_ads_tle_epoch,            ///< Current TLE epoch, 0 if TLE is invalid
    dat_ads_tle_last,             ///< Las time position was propagated
    dat_ads_q0,                   ///< Attitude quaternion (Inertial to body)
    dat_ads_q1,                   ///< Attitude quaternion (Inertial to body)
    dat_ads_q2,                   ///< Attitude quaternion (Inertial to body)
    dat_ads_q3,                   ///< Attitude quaternion (Inertial to body)
    dat_ads_ekf_q0,               ///< Attitude quaternion (Inertial to body)
    dat_ads_ekf_q1,               ///< Attitude quaternion (Inertial to body)
    dat_ads_ekf_q2,               ///< Attitude quaternion (Inertial to body)
    dat_ads_ekf_q3,               ///< Attitude quaternion (Inertial to body)
    dat_tgt_q0,                   ///< Target quaternion (Inertial to body)
    dat_tgt_q1,                   ///< Target quaternion (Inertial to body)
    dat_tgt_q2,                   ///< Target quaternion (Inertial to body)
    dat_tgt_q3,                   ///< Target quaternion (Inertial to body)
    dat_css_1,                    ///< Coarse sun sensor value
    dat_css_2,                    ///< Coarse sun sensor value
    dat_css_3,                    ///< Coarse sun sensor value
    dat_css_4,                    ///< Coarse sun sensor value
    dat_css_5,                    ///< Coarse sun sensor value
    dat_sun_vec_b_x,
    dat_sun_vec_b_y,
    dat_sun_vec_b_z,

    dat_sun_sc_eci_x,
    dat_sun_sc_eci_y,
    dat_sun_sc_eci_z,
    dat_moon_sc_eci_x,
    dat_moon_sc_eci_y,
    dat_moon_sc_eci_z,
    dat_sc_eci_x,
    dat_sc_eci_y,
    dat_sc_eci_z,
    dat_mag_eci_mG_x,
    dat_mag_eci_mG_y,
    dat_mag_eci_mG_z,
    dat_is_dark_orbit,

    dat_calc_attitude,
    dat_time_to_attitude,
    dat_inertia_xx,               ///< Inertia matrix xx
    dat_inertia_yy,               ///< Inertia matrix yy
    dat_inertia_zz,               ///< Inertia matrix zz
    dat_inertia_xy,               ///< Inertia matrix xy
    dat_inertia_xz,               ///< Inertia matrix xz
    dat_inertia_yz,               ///< Inertia matrix yz
    dat_inv_inertia_xx,           ///< Inertia matrix xx
    dat_inv_inertia_yy,           ///< Inertia matrix yy
    dat_inv_inertia_zz,           ///< Inertia matrix zz
    dat_inv_inertia_xy,           ///< Inertia matrix xy
    dat_inv_inertia_xz,           ///< Inertia matrix xz
    dat_inv_inertia_yz,           ///< Inertia matrix yz
    dat_inertia_rw,

    /// RPI: Raspberry Pi's system variables
    dat_rpi_camera,               ///< Status of the camera
    dat_rpi_i2c,                  ///< Status of the I2C
    dat_rpi_leds,                 ///< Status of the LEDs
    dat_rpi_serial,               ///< Status of the Serial
    dat_rpi_spi,                  ///< Status of the SPI

    /// Add a new status variables address here
    //dat_custom,                 ///< Variable description

    /// LAST ELEMENT: DO NOT EDIT
    dat_status_last_address           ///< Dummy element, the amount of status variables
} dat_status_address_t;

///**
// * A 32 bit variable that can be interpreted as int, uint or float
// */
//typedef union value32_u{
//    int32_t i;
//    uint32_t u;
//    float f;
//} value32_t;

///< Define opeartion modes
#define DAT_OBC_OPMODE_NORMAL        (0) ///< Normal operation
#define DAT_OBC_OPMODE_WARN          (1) ///< Fail safe operation
#define DAT_OBC_OPMODE_FAIL          (2) ///< Generalized fail operation
#define DAT_OBC_OPMODE_REF_POINT     (4) ///< Point to vector
#define DAT_OBC_OPMODE_NAD_POINT     (5) ///< Point to nadir
#define DAT_OBC_OPMODE_DETUMB_MAG    (6) ///< Detumbling

///< Define is a variable is config or status
#define DAT_IS_CONFIG 0
#define DAT_IS_STATUS 1

/**
 * A system variable (status or config) with an address, name, type and value
 */
#define MAX_VAR_NAME 24

typedef struct __attribute__((packed)) dat_sys_var {
    uint16_t address;   ///< Variable address or index (in the data storage)
    char name[MAX_VAR_NAME];      ///< Variable name (max 24 chars)
    char type;          ///< Variable type (u: uint, i: int, f: float)
    int8_t status;      ///< Variable is status (1), is config (0), or uninitialized (-1)
    value32_t value;    ///< Variable default value
} dat_sys_var_t;

/**
 * A system variable (status or config) with an address, and value
 * A short version to be sent as telemetry
 */
typedef struct __attribute__((packed)) dat_sys_var_short {
    uint16_t address;   ///< Variable address or index (in the data storage)
    value32_t value;    ///< Variable default value
} dat_sys_var_short_t;

/**
 * List of status variables with address, name, type and default values
 * This list is useful to decide how to store and send the status variables
 */
static const dat_sys_var_t dat_status_list[] = {
        {dat_obc_last_reset,    "obc_last_reset",    'u', DAT_IS_STATUS, 0},         ///< Last reset source
        {dat_obc_hrs_alive,     "obc_hrs_alive",     'u', DAT_IS_STATUS, 0},         ///< Hours since first boot
        {dat_obc_hrs_wo_reset,  "obc_hrs_wo_reset",  'u', DAT_IS_STATUS, 0},         ///< Hours since last reset
        {dat_obc_reset_counter, "obc_reset_counter", 'u', DAT_IS_STATUS, 0},         ///< Number of reset since first boot
        {dat_obc_sw_wdt,        "obc_sw_wdt",        'u', DAT_IS_STATUS, 0},         ///< Software watchdog timer counter
        {dat_obc_temp_1,        "obc_temp_1",        'f', DAT_IS_STATUS, -1},        ///< Temperature value of the first sensor
        {dat_obc_executed_cmds, "obc_executed_cmds", 'u', DAT_IS_STATUS, 0},
        {dat_obc_failed_cmds,   "obc_failed_cmds",   'u', DAT_IS_STATUS, 0},
        {dat_com_count_tm,      "com_count_tm",      'u', DAT_IS_STATUS, 0},         ///< Number of Telemetries sent
        {dat_com_count_tc,      "com_count_tc",      'u', DAT_IS_STATUS, 0},         ///< Number of received Telecommands
        {dat_com_last_tc,       "com_last_tc",       'u', DAT_IS_STATUS, 0},         ///< Unix time of the last received Telecommand
        {dat_fpl_last,          "fpl_last",          'u', DAT_IS_STATUS, 0},         ///< Last executed flight plan (unix time)
        {dat_fpl_queue,         "fpl_queue",         'u', DAT_IS_STATUS, 0},         ///< Flight plan queue length
        {dat_obc_opmode,        "obc_opmode",        'd', DAT_IS_CONFIG, -1},        ///< General operation mode
        {dat_rtc_date_time,     "rtc_date_time",     'd', DAT_IS_CONFIG, -1},        ///< RTC current unix time
        {dat_drp_idx_temp,      "drp_idx_temp",      'u', DAT_IS_STATUS, 0},         ///< Temperature data index
        {dat_drp_ack_temp,      "drp_ack_temp",      'u', DAT_IS_CONFIG, 0},         ///< Temperature data acknowledge
	{dat_drp_stt,           "drp_stt",           'u', DAT_IS_STATUS, 0},         ///< STT data index
        {dat_drp_stt_exp_time,  "drp_stt_exp_time",  'u', DAT_IS_STATUS, 0},         ///< STT data exposure time index
        {dat_drp_stt_gyro,      "drp_stt_gyro",      'u', DAT_IS_STATUS, 0},         ///< STT data gyro index
	{dat_drp_ack_stt,       "drp_ack_stt",       'u', DAT_IS_CONFIG, 0},         ///< Stt data index acknowledge
        {dat_drp_ack_stt_exp_time, "drp_ack_stt_exp_time",'u', DAT_IS_CONFIG, 0},    ///< Stt data exp time index acknowledge
        {dat_drp_ack_stt_gyro,  "drp_ack_stt_gyro",  'u', DAT_IS_CONFIG, 0},         ///< Stt data gyro index acknowledge
	{dat_ads_omega_x,       "ads_omega_x",       'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the x axis
        {dat_ads_omega_y,       "ads_omega_y",       'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the y axis
        {dat_ads_omega_z,       "ads_omega_z",       'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the z axis
        {dat_ads_mag_x,         "ads_mag_x",         'f', DAT_IS_STATUS, -1},        ///< Magnetometer value along the x axis
        {dat_ads_mag_y,         "ads_mag_y",         'f', DAT_IS_STATUS, -1},        ///< Magnetometer value along the y axis
        {dat_ads_mag_z,         "ads_mag_z",         'f', DAT_IS_STATUS, -1},        ///< Magnetometer value along the z axis
        {dat_ads_pos_x,         "ads_pos_x",         'f', DAT_IS_STATUS, -1},        ///< Satellite orbit position x (ECI)
        {dat_ads_pos_y,         "ads_pos_y",         'f', DAT_IS_STATUS, -1},        ///< Satellite orbit position y (ECI)
        {dat_ads_pos_z,         "ads_pos_z",         'f', DAT_IS_STATUS, -1},        ///< Satellite orbit position z (ECI)
        {dat_ads_tle_epoch,     "ads_tle_epoch",     'd', DAT_IS_STATUS, 0},         ///< Current TLE epoch, 0 if TLE is invalid
        {dat_ads_tle_last,      "ads_tle_last",      'u', DAT_IS_STATUS, 0},         ///< Last time position was propagated
        {dat_ads_q0,            "ads_q0",            'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
        {dat_ads_q1,            "ads_q1",            'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
        {dat_ads_q2,            "ads_q2",            'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
	{dat_ads_q3,            "ads_q3",            'f', DAT_IS_STATUS, 1},         ///< Attitude quaternion (Inertial to body)
        {dat_tgt_omega_x,       "tgt_omega_x",       'f', DAT_IS_CONFIG, 0},         ///< Target acceleration value along the x axis
        {dat_tgt_omega_y,       "tgt_omega_y",       'f', DAT_IS_CONFIG, 0},         ///< Target acceleration value along the y axis
        {dat_tgt_omega_z,       "tgt_omega_z",       'f', DAT_IS_CONFIG, 0},         ///< Target acceleration value along the z axis
        {dat_tgt_q0,            "tgt_q0",            'f', DAT_IS_CONFIG, 0},         ///< Target quaternion (Inertial to body)
        {dat_tgt_q1,            "tgt_q1",            'f', DAT_IS_CONFIG, 0},         ///< Target quaternion (Inertial to body)
        {dat_tgt_q2,            "tgt_q2",            'f', DAT_IS_CONFIG, 0},         ///< Target quaternion (Inertial to body)
        {dat_tgt_q3,            "tgt_q3",            'f', DAT_IS_CONFIG, 1},         ///< Target quaternion (Inertial to body)
        {dat_css_1,             "css_1",             'u', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_css_2,             "css_2",             'u', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_css_3,             "css_3",             'u', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_css_4,             "css_4",             'u', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_css_5,             "css_5",             'u', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_sc_eci_x,          "sc_eci_x",          'f', DAT_IS_STATUS, 0},
        {dat_sc_eci_y,          "sc_eci_y",          'f', DAT_IS_STATUS, 0},
        {dat_sc_eci_z,          "sc_eci_z",          'f', DAT_IS_STATUS, 0},
        {dat_sun_sc_eci_x,      "sun_sc_eci_x",      'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_sun_sc_eci_y,      "sun_sc_eci_y",      'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_sun_sc_eci_z,      "sun_sc_eci_z",      'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_moon_sc_eci_x,     "moon_sc_eci_x",     'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_moon_sc_eci_y,     "moon_sc_eci_y",     'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_moon_sc_eci_z,     "moon_sc_eci_z",     'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_mag_eci_mG_x,      "mag_eci_mG_x",      'f', DAT_IS_STATUS, 0},
        {dat_mag_eci_mG_y,      "mag_eci_mG_x",      'f', DAT_IS_STATUS, 0},
        {dat_mag_eci_mG_z,      "mag_eci_mG_x",      'f', DAT_IS_STATUS, 0},
        {dat_is_dark_orbit,     "is_dark_orbit",     'f', DAT_IS_STATUS, 0},
        {dat_sun_vec_b_x,       "sun_vec_b_x",       'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_sun_vec_b_y,       "sun_vec_b_y",       'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
        {dat_sun_vec_b_z,       "sun_vec_b_z",       'f', DAT_IS_STATUS, 0},         ///< Coarse sun sensor value
	{dat_calc_attitude,     "calc_attitude",     'd', DAT_IS_STATUS, 0},
        {dat_time_to_attitude,  "time_to_attitude",  'u', DAT_IS_STATUS, 10},
        {dat_inertia_xx,        "inertia_xx",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_yy,        "inertia_yy",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_zz,        "inertia_zz",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_xy,        "inertia_xy",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_xz,        "inertia_xz",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_yz,        "inertia_yz",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_xx,    "inertia_xx",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_yy,    "inertia_yy",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_zz,    "inertia_zz",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_xy,    "inertia_xy",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_xz,    "inertia_xz",        'f', DAT_IS_STATUS, 0},
        {dat_inv_inertia_yz,    "inertia_yz",        'f', DAT_IS_STATUS, 0},
        {dat_inertia_rw,        "inertia_rw",        'f', DAT_IS_STATUS, 0},
        {dat_ads_ekf_q0,        "ads_ekf_q0",        'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
	{dat_ads_ekf_q1,        "ads_ekf_q1",        'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
        {dat_ads_ekf_q2,        "ads_ekf_q2",        'f', DAT_IS_STATUS, 0},         ///< Attitude quaternion (Inertial to body)
        {dat_ads_ekf_q3,        "ads_ekf_q3",        'f', DAT_IS_STATUS, 1},         ///< Attitude quaternion (Inertial to body)
        {dat_ads_ekf_omega_x,   "ads_ekf_omega_x",   'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the x axis
        {dat_ads_ekf_omega_y,   "ads_ekf_omega_y",   'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the y axis
        {dat_ads_ekf_omega_z,   "ads_ekf_omega_z",   'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the z axis
        {dat_ads_ekf_bias_x,    "ads_ekf_bias_x",    'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the x axis
        {dat_ads_ekf_bias_y,    "ads_ekf_bias_y",    'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the y axis
        {dat_ads_ekf_bias_z,    "ads_ekf_bias_z",    'f', DAT_IS_STATUS, -1},        ///< Gyroscope acceleration value along the z axis
	{dat_rpi_camera,        "rpi_camera",        'u', DAT_IS_STATUS, 0},         ///< Raspberry Pi's camera status
        {dat_rpi_i2c,           "rpi_i2c",           'u', DAT_IS_STATUS, 0},         ///< Raspberry Pi's i2c status
        {dat_rpi_leds,          "rpi_leds",          'u', DAT_IS_STATUS, 0},         ///< Raspberry Pi's LEDs status
        {dat_rpi_serial,        "rpi_serial",        'u', DAT_IS_STATUS, 0},         ///< Raspberry Pi's serial status
        {dat_rpi_spi,           "rpi_spi",           'u', DAT_IS_STATUS, 0},         ///< Raspberry Pi's spi status
};
///< The dat_status_last_var constant serves for looping through all status variables
static const int dat_status_last_var = sizeof(dat_status_list) / sizeof(dat_status_list[0]);


/**
 * PAYLOAD DATA DEFINITIONS
 */

/**
 * Struct for storing temperature data.
 */
typedef struct __attribute__((__packed__)) temp_data {
    uint32_t index;
    uint32_t timestamp;
    float obc_temp_1;
} temp_data_t;

/**
 * STT structs
 */
typedef struct __attribute__((__packed__)) stt_data {
    uint32_t index;
    uint32_t timestamp;
    float ra;
    float dec;
    float roll;
    int time;
    float exec_time;
} stt_data_t;

typedef struct __attribute__((__packed__)) stt_exp_time_data{
    uint32_t index;
    uint32_t timestamp;
    int exp_time;
    int n_stars;
}stt_exp_time_data_t;

typedef struct __attribute__((__packed__)) stt_gyro_data{
    uint32_t index;
    uint32_t timestamp;
    float gx;
    float gy;
    float gz;
}stt_gyro_data_t;

/**
 * Enum constants for dynamically identifying payload fields at execution time.
 *
 * Also permits adding payload fields cheaply.
 *
 * The last_sensor constant serves only for comparison when looping through all
 * payload values. For example:
 *
 * @code
 * for (payload_id_t i = 0; i < last_sensor; i++)
 * {
 * // some code using i.
 * }
 * @endcode
 */
typedef enum payload_id {
    temp_sensors=0,         ///< Temperature sensors
    stt_sensors,            ///< STT sensors
    stt_exp_time_sensors,   ///< STT exposure time sensors
    stt_gyro_sensors,       ///< STT gyro sensor
    last_sensor             ///< Dummy element, the amount of payload variables
} payload_id_t;

/**
 * Struct for storing data collected by status variables.
 */
typedef struct __attribute__((__packed__)) sta_data {
    uint32_t index;
    uint32_t timestamp;
    uint32_t sta_buff[sizeof(dat_status_list) / sizeof(dat_status_list[0])];
} sta_data_t;

static data_map_t data_map[] = {
    {"temp_data",      (uint16_t) (sizeof(temp_data_t)), dat_drp_idx_temp, dat_drp_ack_temp, "%u %u %f", "sat_index timestamp obc_temp_1"},
    {"stt_data",       (uint16_t) (sizeof(stt_data_t)), dat_drp_stt, dat_drp_ack_stt, "%u %u %f %f %f %d %f",
            "sat_index timestamp ra dec roll time exec_time"},
    {"stt_exp_time",   (uint16_t) (sizeof(stt_exp_time_data_t)), dat_drp_stt_exp_time, dat_drp_ack_stt_exp_time, "%u %u %d %d",
            "sat_index timestamp exp_time n_stars"},
    {"stt_gyro_data", (uint16_t) (sizeof(stt_gyro_data_t)), dat_drp_stt_gyro, dat_drp_ack_stt_gyro, "%u %u %f %f %f",
            "sat_index, timestamp gx gy gz"},
};

/** The repository's name */
#define DAT_TABLE_STATUS "dat_status"      ///< Status variables table name
#define DAT_TABLE_DATA   "dat_data"        ///< Data storage table name
#define DAT_TABLE_FP     "dat_flightplan"  ///< Flight plan table name

/**
 * Search and return a status variable definition from dat_status_list by index or by name
 * @param address Variable index
 * @param name Variable name
 * @return dat_sys_var_t or 0 if not found.
 */
dat_sys_var_t dat_get_status_var_def(dat_status_address_t address);
dat_sys_var_t dat_get_status_var_def_name(char *name);

/**
 * Print the names and values of a system status variable list.
 * @param status Pointer to a status variables list
 */
void dat_print_system_var(dat_sys_var_t *status);

#endif //REPO_DATA_SCHEMA_H
