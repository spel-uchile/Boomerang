/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *     Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app/system/cmdRPI.h"
#include "app/system/taskCAM.h"

static const char *tag = "\e[38;2;246;178;107mCAM\x1b[0m";

void taskCAM(void *param) {
    osDelay(50);
    LOGI(tag, "Started");
    int sec_to_min = 60;
    portTick xLastWakeTime = osTaskGetTickCount();
    portTick task_period = 60*60*1000; //Task period in [ms]
    while (1) {
        int64_t t0 = dat_get_time();
        osDelay(11000*sec_to_min);
        LOGI(tag, "Turn on white LEDs");
        cmd_t *leds_cmd = cmd_build_from_str("rpi_system 10 python3 ~/extremofilos_iss/Software/led_control/led_control.py >&- 2>&- &");
        cmd_send(leds_cmd);
        while (dat_get_time() - t0 < 15000*sec_to_min) {
            cmd_t *cam_cmd = cmd_build_from_str("rpi_system 10 python3 ~/multiplexer_camera/scripts/quad_camera.py $(date +~/Pictures/CAM/cam_%Y%m%d_%H%M%S) 0 >&- 2>&- &");
            cmd_send(cam_cmd);
	    LOGI(tag, "Taking multicamera pictures");
	    osDelay(10000);
	}
        LOGI(tag, "Turn off white LEDs");
	//osDelay(3000*sec_to_min);
        osTaskDelayUntil(&xLastWakeTime, task_period);
    }
}
