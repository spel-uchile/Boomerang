# Boomerang

This repository contains the code that will operate in our next mission inside the ISS.

## Getting started

First run the following command to download some files and repositories:

```shell
sudo apt-get install git -y
git clone https://gitlab.com/spel-uchile/Boomerang.git
sh Boomerang/download_repos.sh
```

Now you need to install some dependencies:

```shell
sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install bc cmake gcc i2c-tools libcap2 libcap-dev libpq-dev libsqlite3-dev libusb-1.0-0-dev libzmq3-dev make ninja-build pkg-config python3-pip python3-smbus python3-dev vim -y
wget https://www.python.org/ftp/python/2.7.18/Python-2.7.18.tgz
tar zxvf Python-2.7.18.tgz
cd Python-2.7.18
sudo ./configure
make
sudo make install
cd ../Boomerang/
pip3 install -r requirements.txt --break-system-packages
comm -23 <(sort requirements.txt) <(sort installed_packages.txt) | xargs -n 1 pip3 install
cd ../suchai-framework-stt/
sh stt_installer.sh
sh extract_cat.sh
sudo [ ! -f "/etc/udev/rules.d/pco_usb.rules" ] && sudo mkdir -p "/etc/udev/rules.d" && echo 'SUBSYSTEM=="usb" , ATTR{idVendor}=="1cb2" , GROUP="video" , MODE="0666" , SYMLINK+="pco_usb_camera%n"' | sudo tee /etc/udev/rules.d/pco_usb.rules > /dev/null && udevadm trigger || true
cd
mkdir Documents Pictures Videos
cd Documents/
mkdir DFH MAG PIX POW RAD SEU STT TOP
cd ../Pictures/
mkdir CAM PCO
```

After installing all the dependencies, you can combine all the applications of
the SUCHAI flight software inside one application running the update_repositories.sh
bash script. You can also use it to update the other repositories and copy the
new updates into the SUCHAI flight software:

```shell
cd ../Boomerang/
sh update_repositories.sh
```

If you want to compile our flight software, you only need to move to its repository
and run a bash script as follows:

```shell
cd ../suchai-flight-software/
sh build.sh
```

## USB Power

The payloads connected to USB consume more than 600 mA in some ports. For this reason, in the case of the Raspberry Pi 5, we modify a configuration file to increase the power limit. To do this, please open the file /boot/firmware/config.txt and add the following line at the bottom:

```shell
usb_max_current_enable=1
```

To turn off the USB power for all the 4 ports in the Raspberry Pi, you need to run the following commands:

```shell
cd ../Boomerang
sudo sh powerdown_usbhub.sh
```

To turn it back on you need these commands:

```shell
sudo sh powerup_usbhub.sh
```

If it is necessary to have the USB ports powered down when booting up, then follow the next instructions:

```shell
cd ../Boomerang/
sudo cp usb_power.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable usb_power.service
cd ../suchai-flight-software/
```
