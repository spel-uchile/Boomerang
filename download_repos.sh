#!/usr/bin/env bash
git clone -b feature/time_in_ms https://gitlab.com/spel-uchile/suchai-flight-software.git
git clone -b framework https://gitlab.com/samuel-gutierrez/suchai-framework-stt.git
git clone -b framework https://gitlab.com/spel-uchile/suchai-software-template.git
git clone https://gitlab.com/spel-uchile/pco-iss.git
git clone https://gitlab.com/felipeedh/multiplexer_camera.git
git clone https://gitlab.com/felipeedh/extremofilos_iss.git
git clone https://gitlab.com/samuel-gutierrez/minipix.git
git clone -b framework https://gitlab.com/EliasObreque/suchai-software-adcs-test.git
git clone https://gitlab.com/spel-uchile/RPI_Commands.git
git clone https://github.com/Japm-Engineer/LiquidLens.git
git clone https://github.com/mvp/uhubctl.git
git clone https://gitlab.com/jdiazpena/mag-plus-jmdp.git
git clone -b framework https://gitlab.com/alexbecerra/suchai-software-grafeno.git
cd suchai-software-grafeno/apps/grafeno/src
git clone -b fix-v1 https://gitlab.com/alexbecerra/Graf_Py_drivers.git
cd ../../../../
